package Utilidades;

import java.util.Arrays;

public class Ordenacion{

	//BIEN
	public static void Ordenacion_por_seleccion(int[] vector) {	
		int min,aux;
		
		for(int i=0;i<vector.length;i++){
			min=i;
			for(int j=i+1;j<vector.length;j++)
				if(vector[j]<vector[min])
					min=j;
			if(min!=i){
				aux=vector[i];
				vector[i]=vector[min];
				vector[min]=aux;
			}			
		}		
		System.out.println(Arrays.toString(vector));
	}
	
	//BIEN
	public static void MetodoIntercambio(int[] vector){
		int aux;
		
		for(int i=0;i<vector.length;i++)
			for(int j=i;j<vector.length;j++)
				if(vector[j]<vector[i]){
					aux = vector[i];
					vector[i] = vector[j];
					vector[j] = aux;
				}
		System.out.println(Arrays.toString(vector));
	}
	
	//BIEN
	public static void Ordenacion_por_insercion(int[] vector){
		int aux,i;
		
		for(int j=0;j<=vector.length-1;j++){
			aux = vector[j];
			i=j-1;
			while(i>=0 && vector[i]>aux){
				vector[i+1]=vector[i];
				i=i-1;
			}
			vector[i+1]=aux;
		}
		System.out.println(Arrays.toString(vector));
	}
	
	//BIEN
	public static void MetodoBurbuja(int[] vector){
		int aux;
		
		for(int i=0;i<(vector.length-1);i++)
			for(int j=0;j<(vector.length-1)-i;j++)
				if(vector[j]>vector[j+1]){
					aux = vector[j];
					vector[j]=vector[j+1];
					vector[j+1]=aux;
				}
		System.out.println(Arrays.toString(vector));
	}

	//BIEN
	public static void MetodoBurbujaMejorado(int[] vector){
		int i,aux;
		boolean cambios=true;	
		i=0;
		
			
		while(cambios){
			cambios=false;
			i++;
			for(int j=0;j<vector.length-i;j++)
				if(vector[j]>vector[j+1]){
					aux=vector[j];
					vector[j]=vector[j+1];
					vector[j+1]=aux;
					cambios=true;
				}
		}
		System.out.println(Arrays.toString(vector));		
	}
	
	public static void main(String[] args){
		
		int a[] = {1,6,10,-4,3};
		int b[] = {1,6,10,-4,3};
		int c[] = {1,6,10,-4,3};
		int d[] = {1,6,10,-4,3};
		int e[] = {1,6,10,-4,3};
		//Ordenacion_por_seleccion(a);
		//MetodoIntercambio(b);
		//Ordenacion_por_insercion(c);
		//MetodoBurbuja(d);
		//MetodoBurbujaMejorado(e);
		
	}
}